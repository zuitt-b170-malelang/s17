/* Activity:
1. In the S17 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create an addStudent() function that will accept a name of the student and add it to the student array.
4. Create a countStudents() function that will print the total number of students in the array.
5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.
7. Create a git repository named S17.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle. */

let students=[]

function addStudent(student){
  students.push(student);
};

function countStudents(){
  console.log("The number of students enrolled is " + (students.length));
};

function printStudents(){
  students.sort(
    function (a, b){
      return a - b
    }
  )
  students.forEach(
    function(student){
      console.log(student)
    }
  );
}

function findStudent(student){
  let newFind  = students.filter(
    function (element){
      return element === student
    }
  );
  if(newFind.length>1){
    console.log(student + " are enrollees.")
  }
  else if(newFind.length=1){
    console.log(student + " is an enrollee.")
  }
  else{
    console.log(student + " is not an enrollee.")
  }
  }

function addSection(section){
  let mapSection = students.map(
    function(element){
      return ` ${element}- Section ${section}`
    }
  )
  console.log(mapSection)
}

function removeStudent(student){
  let slicedstudents = student.slice(0, 1);
  let slicedstudents2 = student.slice(1, student.length)
  let newStudent = slicedstudents.toUpperCase() + slicedstudents2;


let removed = students.splice(students.indexOf(newStudent), 1)
  console.log(student + " was removed from the class")

}
