/* 
How do we display the following tasks in the console

1. drink html
2. eat javascript
3. inhale css
4. bake bootstrap

take screenshot of your output and send it in the google chat
*/


/* 
  arrays - used to store multiple related data values inside a single variable. Arrays are declared using the square brackets ([])

  SYNTAX:
    let/const <arrayName> = ["element1", "element2"....]

  arrays are used if there is a need to manipulate the related values stored in it


  index - position of each element in the array

  REMINDER: the index always starts with 0. counting the elements inside would start with 0 insted of 1
    formula: -nth element -1/ arrayName.length - 1
*/

let tasks = ["drink HTML", "eat javascript", "inhale css", "bake bootstrap"];

console.log(tasks);

// getting the element through the array's index


console.log(tasks[2]);
console.log(tasks.length)

// ===============================

// Array Manipulation

// ADDING AN ELEMENT
let numbers = ["one", "two", "three", "four"];
console.log(numbers);

// using assignment operator
numbers[4] = "five";
console.log(numbers);

// Push Method
// add an element/s at the end of the array
numbers.push("element")
console.log(numbers);
// callback function
// push methoud using callback function
// a function that is passed on to another function. This is done because the inserted function is following a particular syntax and the developer is trying to simplify the syntax by just inserting it inside another function
function pushMethod(element) {
  numbers.push(element);
};
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);

//REMOVING OF AN ELEMENT
//pop method
// removes the element at the end of the array (last element)

numbers.pop();
console.log(numbers);

function popMethod() {
  numbers.pop()
};
popMethod();
console.log(numbers);

// ==========================
// MANIPULATING THE BEGINNING?START OF THE ARRAY
// Remove the first element
// Shift Method
// removes the first element of the array

numbers.shift();
console.log(numbers);

// callback
function shiftMethod() {
  numbers.shift();
}
shiftMethod();
console.log(numbers);

// ADDING AN ELEMENT
// unshift Method
numbers.unshift("zero");
console.log(numbers);

// callback function
function unshiftMethod(element) {
  numbers.unshift(element);
};
unshiftMethod("mcdo");
console.log(numbers);

// ARRANGEMENT OF THE ELEMENTS

let numbs = [15, 27, 32, 12, 6, 8, 111,236]
console.log(numbs);
// sort method - arranges the elements in ascending or descending order
  // has an anonymous function inside that has 2 parameters

  // anonymous function - unnamed function and can only be used once

/* 
  2 parameters inside the anonymous function represents:
  first parameter - first  smallest/starting element
  second parameter - last/biggest/ending element
*/
/* 
SYNTAX:
arrayName.sort(
  function(a, b){
    return a - b >>ascending
    return b - a >> descending
  }
)
*/
numbs.sort(
  function (a, b) {
    return a - b
  }
);
console.log(numbs);

// descending order
numbs.sort(
  function(a,b){
    return b-a
  }
);
console.log(numbs);

// reverse method
  // reverses the order of the elements in an array
numbs.reverse();
console.log(numbs);

// ===========================
// Splice Method (cut paste)
/* 
  -directly manipulates the array
  -first parameter - the index of the element from which the omitting will begin
  -second parameter - determines the number of elements to be omitted
  -third parameter onwards - the replacements for the removed elements
*/

// Only one parameter (pure omission)
// let nums = numbs.splice(1)

// two parameters (pure omission)
/* let nums = numbs.splice(1,2)
console.log(numbs);
console.log(nums); */

// three parameters (replacements)
let nums = numbs.splice(4,2, 31, 11)
console.log(numbs);
console.log(nums);

// ============================
// Slice Method (copy paste)
/* 
  It does not affect the original array

  -first parameter - index where copying will begin

  -second parameter - the number of elements to be copied starting from the first element (copying will still begin from the first parameter)

  -third onwards - the replacements for
*/
// One parameter
/* let slicedNums = numbs.slice(4);
console.log(numbs);
console.log(slicedNums); */

// two parameters
let slicedNums = numbs.slice(4, 6);
console.log(numbs);
console.log(slicedNums);

// ==========================
// Merging of Array
// Concat
console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);

let newConcat= numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

// join method - merges the elements inside the array and makes them string data
// parameters - separators (space, dash and nothing at all)
let meal = ["rice", "steak", "juice"];
console.log(meal)

let newJoin = meal.join();
console.log(newJoin);

// toString method
console.log(nums);
// typeof determines the data type
console.log(typeof nums);

let newString = numbs.toString();
console.log(newString);
console.log(typeof newString)

// Accessors

let countries = ["US", "PH", "JP", "HK", "SG","PH", "NZ"];
// indexOf
let index = countries.indexOf("PH");
console.log(index);
// finding a non-existing element
index = countries.indexOf("AU");
console.log(index);

// lastindexOf() - finds the index of the element starting from the end of the array
index = countries.lastIndexOf("PH");
console.log(index);

// Iterators
// forEach

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
console.log(days);

// forEach
/* SYNTAX:
array.forEach(
  function(element){
    statement/s
  }
) */
days.forEach(
  function(element){
    console.log(element)
  }
)


// map
/* 
SYNTAX:
array.map(
  function(element){
    statement/s
  }
)
*/
  // returns a copy of an array from the original which can be manipulated
let mapDays = days.map(
  function(element){
    return `${element} is the day of the week`
  }
);
console.log(mapDays);
console.log(days);


// filter
console.log(numbs);
let newFilter = numbs.filter(
  function (element){
    return element<30
  }
);
console.log(newFilter);
console.log(numbs);


// includes - returns true (boolean) if the element/s are inside the array
let animalIncludes = animals.includes("dog");
console.log(animalIncludes);

// every - checks if all the element pass the condition.
let newEvery = nums.every(
  function(element){
    return (element>10)
  }
);
console.log(newEvery);
console.log(nums)

// Some
let newSome = nums.some(
  function(element){
    return (element > 30);
  }
);
console.log(newSome);

nums.push(50)
// reduce
let newReduce = nums.reduce(
  function(a, b){
    return a + b;
  }
)
console.log(newReduce);

let average = newReduce/nums.length;
console.log(average);

// toFixed - sets the number of decimal places

/* 
  parseInt - rounds the number to the neares whole number

  parseFloat - rounds the number to the nearest target decimal place( through the use of .toFixed)

*/
console.log(average);
console.log(average.toFixed(2));
console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));